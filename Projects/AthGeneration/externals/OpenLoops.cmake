#
# File specifying the location of OpenLoops to use.
#

set( OPENLOOPS_LCGVERSION "2.0.0" )
set( OPENLOOPS_LCGROOT
  "${LCG_RELEASE_DIR}/MCGenerators/openloops/${OPENLOOPS_LCGVERSION}/${LCG_PLATFORM}" )